<?php

namespace App\Http\Controllers;

use App\Models\Bootcamp;
use Illuminate\Http\Request;

class BootcampController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        echo 'aqui se muestra una lista de boostcamp';
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        echo 'aqui creare el bootcamp';
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Bootcamp  $bootcamp
     * @return \Illuminate\Http\Response
     */
    public function show(Bootcamp $bootcamp)
    {
        echo "aqui se mostrara un bootcamp por id: $id";
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Bootcamp  $bootcamp
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Bootcamp $bootcamp)
    {
        echo "aqui se va a actulizar un bootcamp cuyo id es: $id";
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Bootcamp  $bootcamp
     * @return \Illuminate\Http\Response
     */
    public function destroy(Bootcamp $bootcamp)
    {
        echo "aqui se va a eliminar un bootcamp cuyo id es: $id";
    }
}
